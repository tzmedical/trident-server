## [2.2.1](https://bitbucket.org/tzmedical/trident-server/compare/v2.2.0...v2.2.1) (2023-09-21)

# [2.2.0](https://bitbucket.org/tzmedical/trident-server/compare/v2.1.2...v2.2.0) (2023-09-20)


### Features

* adding `/parse` routes for each file type ([3bfd423](https://bitbucket.org/tzmedical/trident-server/commits/3bfd42342d326f07f79edb785b70a29f96b143e8))

## [2.1.2](https://bitbucket.org/tzmedical/trident-server/compare/v2.1.1...v2.1.2) (2022-11-16)

## [2.1.2-alpha.1](https://bitbucket.org/tzmedical/trident-server/compare/v2.1.1...v2.1.2-alpha.1) (2022-11-16)

## [2.1.1](https://bitbucket.org/tzmedical/trident-server/compare/v2.1.0...v2.1.1) (2022-11-15)


### Bug Fixes

* update hpa to use v2 api instead of v2beta2 ([1c35782](https://bitbucket.org/tzmedical/trident-server/commits/1c35782adcdfa63beaf624a9e4f123fae3b12ab7))

# [2.1.0](https://bitbucket.org/tzmedical/trident-server/compare/v2.0.0...v2.1.0) (2022-11-15)


### Bug Fixes

* add uri attribute to unit test ([0f8f4de](https://bitbucket.org/tzmedical/trident-server/commits/0f8f4de88f3c640cbd04354a60b12971ebf98496))


### Features

* add uri to pubsub attributes on trident-server-file-received ([e40841e](https://bitbucket.org/tzmedical/trident-server/commits/e40841e1d8176b6a00a3198df65de4c7fe48b5fd))

# [2.0.0](https://bitbucket.org/tzmedical/trident-server/compare/v1.13.9...v2.0.0) (2022-11-08)


### Bug Fixes

* fixing server.listen command to respect the selected NODE_PORT ([2ba973e](https://bitbucket.org/tzmedical/trident-server/commits/2ba973e7fd7482a928338b1deb55aa0f91a12c27))


### Features

* upgrading npm packages to latest versions ([f4a8b9c](https://bitbucket.org/tzmedical/trident-server/commits/f4a8b9c3a1bf009da35fcc4752e660cfffed50b6))


### BREAKING CHANGES

* Now using the latest version of trident-sdk with different names for certain JSON
fields.

# [2.0.0-alpha.2](https://bitbucket.org/tzmedical/trident-server/compare/v2.0.0-alpha.1...v2.0.0-alpha.2) (2022-11-04)

# [2.0.0-alpha.1](https://bitbucket.org/tzmedical/trident-server/compare/v1.13.9...v2.0.0-alpha.1) (2022-11-04)


### Bug Fixes

* fixing server.listen command to respect the selected NODE_PORT ([2ba973e](https://bitbucket.org/tzmedical/trident-server/commits/2ba973e7fd7482a928338b1deb55aa0f91a12c27))


### Features

* upgrading npm packages to latest versions ([f4a8b9c](https://bitbucket.org/tzmedical/trident-server/commits/f4a8b9c3a1bf009da35fcc4752e660cfffed50b6))


### BREAKING CHANGES

* Now using the latest version of trident-sdk with different names for certain JSON
fields.

## [1.13.9](https://bitbucket.org/tzmedical/trident-server/compare/v1.13.8...v1.13.9) (2022-10-21)

## [1.13.8](https://bitbucket.org/tzmedical/trident-server/compare/v1.13.7...v1.13.8) (2022-10-21)


### Bug Fixes

* fixes bug with attempting to write to a redis replica ([059885f](https://bitbucket.org/tzmedical/trident-server/commits/059885fa4bc797c784f7c3ccaedbb8d326494de4))

## [1.13.7](https://bitbucket.org/tzmedical/trident-server/compare/v1.13.6...v1.13.7) (2022-09-13)

## [1.13.6](https://bitbucket.org/tzmedical/trident-server/compare/v1.13.5...v1.13.6) (2022-08-05)

## [1.13.5](https://bitbucket.org/tzmedical/trident-server/compare/v1.13.4...v1.13.5) (2022-07-29)

## [1.13.4](https://bitbucket.org/tzmedical/trident-server/compare/v1.13.3...v1.13.4) (2022-07-29)

## [1.13.3](https://bitbucket.org/tzmedical/trident-server/compare/v1.13.2...v1.13.3) (2022-07-27)

## [1.13.2](https://bitbucket.org/tzmedical/trident-server/compare/v1.13.1...v1.13.2) (2022-07-17)

## [1.13.1](https://bitbucket.org/tzmedical/trident-server/compare/v1.13.0...v1.13.1) (2022-07-12)

# [1.13.0](https://bitbucket.org/tzmedical/trident-server/compare/v1.12.3...v1.13.0) (2022-07-12)


### Features

* adding Google pubsub as an alternative to the REST API for service communication ([e6380ff](https://bitbucket.org/tzmedical/trident-server/commits/e6380ff827b5efaac212477a777f355cb42eb7f1))
* **pubsub:** adding payload validation and sanitization prior to storage ([0e9146b](https://bitbucket.org/tzmedical/trident-server/commits/0e9146b85e87abf0672e0fdddfc0f298efac93f7))

# [1.13.0-alpha.1](https://bitbucket.org/tzmedical/trident-server/compare/v1.12.3...v1.13.0-alpha.1) (2022-07-12)


### Features

* adding Google pubsub as an alternative to the REST API for service communication ([e6380ff](https://bitbucket.org/tzmedical/trident-server/commits/e6380ff827b5efaac212477a777f355cb42eb7f1))
* **pubsub:** adding payload validation and sanitization prior to storage ([0e9146b](https://bitbucket.org/tzmedical/trident-server/commits/0e9146b85e87abf0672e0fdddfc0f298efac93f7))

# [1.13.0-alpha.1](https://bitbucket.org/tzmedical/trident-server/compare/v1.12.3...v1.13.0-alpha.1) (2022-07-12)


### Features

* adding Google pubsub as an alternative to the REST API for service communication ([e6380ff](https://bitbucket.org/tzmedical/trident-server/commits/e6380ff827b5efaac212477a777f355cb42eb7f1))
* **pubsub:** adding payload validation and sanitization prior to storage ([0e9146b](https://bitbucket.org/tzmedical/trident-server/commits/0e9146b85e87abf0672e0fdddfc0f298efac93f7))

# [1.13.0](https://bitbucket.org/tzmedical/trident-server/compare/v1.12.3...v1.13.0) (2022-07-12)


### Features

* adding Google pubsub as an alternative to the REST API for service communication ([e6380ff](https://bitbucket.org/tzmedical/trident-server/commits/e6380ff827b5efaac212477a777f355cb42eb7f1))
* **pubsub:** adding payload validation and sanitization prior to storage ([0e9146b](https://bitbucket.org/tzmedical/trident-server/commits/0e9146b85e87abf0672e0fdddfc0f298efac93f7))

## [1.12.3](https://bitbucket.org/tzmedical/trident-server/compare/v1.12.2...v1.12.3) (2022-06-28)

## [1.12.3-alpha.3](https://bitbucket.org/tzmedical/trident-server/compare/v1.12.3-alpha.2...v1.12.3-alpha.3) (2022-06-27)

## [1.12.3-alpha.2](https://bitbucket.org/tzmedical/trident-server/compare/v1.12.3-alpha.1...v1.12.3-alpha.2) (2022-06-27)

## [1.12.3-alpha.1](https://bitbucket.org/tzmedical/trident-server/compare/v1.12.2...v1.12.3-alpha.1) (2022-06-27)

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.12.2](https://bitbucket.org/tzmedical/trident-server/compare/v1.12.1...v1.12.2) (2022-05-23)

### [1.12.1](https://bitbucket.org/tzmedical/trident-server/compare/v1.12.0...v1.12.1) (2022-05-06)

## [1.12.0](https://bitbucket.org/tzmedical/trident-server/compare/v1.11.0...v1.12.0) (2022-04-21)


### Features

* added filtering queued items by deviceEnrollmentId ([33de5d8](https://bitbucket.org/tzmedical/trident-server/commit/33de5d8bfade640fac737359d5f7bc9ae1c78fa5))

## [1.11.0](https://bitbucket.org/tzmedical/trident-server/compare/v1.10.4...v1.11.0) (2022-04-19)


### Features

* adding error log parsing support ([6f39e7d](https://bitbucket.org/tzmedical/trident-server/commit/6f39e7d2122e2fe2cb055cc0f24c16d30686fa7d))

### [1.10.4](https://bitbucket.org/tzmedical/trident-server/compare/v1.10.3...v1.10.4) (2022-04-19)

### [1.10.3](https://bitbucket.org/tzmedical/trident-server/compare/v1.10.2...v1.10.3) (2022-04-15)

### [1.10.2](https://bitbucket.org/tzmedical/trident-server/compare/v1.10.1...v1.10.2) (2022-04-15)

### [1.10.1](https://bitbucket.org/tzmedical/trident-server/compare/v1.10.0...v1.10.1) (2022-04-15)

## [1.10.0](https://bitbucket.org/tzmedical/trident-server/compare/v1.9.2...v1.10.0) (2022-04-14)


### Features

* adding support for space-separated lists of URLs ([c263f25](https://bitbucket.org/tzmedical/trident-server/commit/c263f258daa65ba832fa88c05095f5d0d7166607))


### Bug Fixes

* fixing a typo in the URL regex ([4d1980d](https://bitbucket.org/tzmedical/trident-server/commit/4d1980d0f1af23b169b60bd8d0d801119c21eab6))

### [1.9.2](https://bitbucket.org/tzmedical/trident-server/compare/v1.9.1...v1.9.2) (2022-04-13)

### [1.9.1](https://bitbucket.org/tzmedical/trident-server/compare/v1.9.0...v1.9.1) (2022-04-13)

## [1.9.0](https://bitbucket.org/tzmedical/trident-server/compare/v1.8.1...v1.9.0) (2022-04-13)


### Features

* changing the log level on the `/` route ([118def1](https://bitbucket.org/tzmedical/trident-server/commit/118def1776dedd425d37e193463ad105d80f32e0))

### [1.8.1](https://bitbucket.org/tzmedical/trident-server/compare/v1.8.0...v1.8.1) (2022-04-11)

## [1.8.0](https://bitbucket.org/tzmedical/trident-server/compare/v1.7.0...v1.8.0) (2022-04-11)


### Features

* adding API to queue actions and settigns to send to devices ([f6f58e5](https://bitbucket.org/tzmedical/trident-server/commit/f6f58e5db5fef9256f6f7147224de8b51b7edb1b))
* returning queued actions and settings in response to device requests ([8239f1a](https://bitbucket.org/tzmedical/trident-server/commit/8239f1aff9a7f9b38bc3e2d505ef6a2743127999))

## [1.7.0](https://bitbucket.org/tzmedical/trident-server/compare/v1.6.0...v1.7.0) (2022-04-05)


### Features

* adding redis database to helm dependencies ([6201d1a](https://bitbucket.org/tzmedical/trident-server/commit/6201d1a132890c5ee5446e86ee91aa6c85ae7e79))

## [1.6.0](https://bitbucket.org/tzmedical/trident-server/compare/v1.5.0...v1.6.0) (2022-01-10)


### Features

* improving sanitization of POST URL lists ([9d87c1f](https://bitbucket.org/tzmedical/trident-server/commit/9d87c1f122b82c6510070b9f759bfcaafd39b69d))

## [1.5.0](https://bitbucket.org/tzmedical/trident-server/compare/v1.4.0...v1.5.0) (2022-01-08)


### Features

* adding HTTP POST forwarding for parsed files ([c7cc169](https://bitbucket.org/tzmedical/trident-server/commit/c7cc1697dcf323064a7e8b84796a085722ebf0dd))


### Bug Fixes

* fixing the program name in the cli to be correct ([46743ed](https://bitbucket.org/tzmedical/trident-server/commit/46743ed895d8f6685c346be985bae3c0ad62c294))

## [1.4.0](https://bitbucket.org/tzmedical/trident-server/compare/v1.3.2...v1.4.0) (2022-01-03)


### Features

* adding container deployment support ([25e9d7f](https://bitbucket.org/tzmedical/trident-server/commit/25e9d7f78df7fca323d4497a2c9fcbff0c51db60))

### [1.3.2](https://bitbucket.org/tzmedical/trident-server/compare/v1.3.1...v1.3.2) (2021-12-21)

### [1.3.1](https://bitbucket.org/tzmedical/trident-server/compare/v1.3.0...v1.3.1) (2021-12-21)

## [1.3.0](https://bitbucket.org/tzmedical/trident-server/compare/v1.2.0...v1.3.0) (2021-12-21)


### Features

* returning an empty body for all device routes to avoid device errors ([693ddad](https://bitbucket.org/tzmedical/trident-server/commit/693ddad6a3ddd8a633b8e69eb9813507753ffecd))

## [1.2.0](https://bitbucket.org/tzmedical/trident-server/compare/v1.1.0...v1.2.0) (2021-12-21)


### Features

* **cli:** adding a cli for starting the server ([e2b5e33](https://bitbucket.org/tzmedical/trident-server/commit/e2b5e33a98b46c9c5a705c556e6b6a9801fea291))

## [1.1.0](https://bitbucket.org/tzmedical/trident-server/compare/v1.0.1...v1.1.0) (2021-12-20)


### Features

* adding trident-sdk for parsing incoming payloads ([c15b17d](https://bitbucket.org/tzmedical/trident-server/commit/c15b17dc2081cd30d3c37f6505828fb38eb07bb6))

### 1.0.1 (2021-12-20)
