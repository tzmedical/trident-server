FROM node:lts-alpine

ARG NODE_ENV=production
ENV NODE_ENV=$NODE_ENV
WORKDIR /var/www

COPY package*.json /var/www/
RUN npm install --legacy-peer-deps

COPY . /var/www/

ARG START_CMD="node"
RUN echo -e "#!/bin/sh\n${START_CMD} cli.js start" > startup.sh \
  && chmod +x startup.sh
CMD ["./startup.sh"]
