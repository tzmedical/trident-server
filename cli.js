#!/usr/bin/env node
/* eslint-env node */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------
const { program } = require('commander');
const pack = require('./package.json');
const app = require('./server');

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

// ==============================================================================
const commaSeparatedList = (value) => {
  try {
    return JSON.parse(value);
  } catch (err) {
    return value.split(/[,[\]]/).filter((el) => el.length);
  }
};

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

// ==============================================================================
program.name('trident-server').version(pack.version);

// ==============================================================================
program
  .command('start')
  .usage('')
  .option('-p, --port <port>', 'the port for the server to listen on')
  .option(
    '--post <urlList>',
    'set a list of URLs to POST parsed files to, as a comma-separated list',
    commaSeparatedList
  )
  .option(
    '--redis-write <redisMaster>',
    'the hostname of a Redis database to use for queueing actions. Also used for reads if --redis-replicas is not set.'
  )
  .option(
    '--redis-read <redisReplicas>',
    'the hostname of a Redis database replica to use for checking queued actions.'
  )
  .option(
    '--redis-password <redisPassword>',
    'the password to authenticate with the Redis database, if needed.'
  )
  .option('--pubsub-enabled', 'use to enable google-pubsub for service communication')
  .option(
    '--pubsub-received',
    'use to override the default topic name of trident-server-file-received'
  )
  .option(
    '--pubsub-actions',
    'use to override the default topic name of trident-server-actions-queue'
  )
  .option(
    '--pubsub-subscription',
    'use to override the default subscription name of trident-server-actions-queue'
  )
  .option('--log-level <logLevel>', 'the logging level for the server to use')
  .option('--pretty-logs', 'set to use pretty-print formatting on logs while developing')
  .description('Start the Trident Server')
  .action((options) => {
    app({
      port: options.port,
      logLevel: options.logLevel,
      prettyLogs: !!options.prettyLogs,
      postList: options.post,
      redisMaster: options.redisMaster,
      redisReplicas: options.redisReplicas,
      redisPassword: options.redisPassword,
      pubsub: {
        enabled: options.pubsubEnabled,
        receivedTopic: options.pubsubReceived,
        actionsTopic: options.pubsubActions,
        subscription: options.pubsubSubscription,
      },
    });
  });

program.parse(process.argv);

if (!program.args.length) {
  program.help();
}
