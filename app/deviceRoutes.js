/* eslint-env node */
/* eslint-disable no-unused-vars */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

const { tze, tzr, scp, error } = require('@tzmedical/trident-sdk');
const { forwardParsedFile } = require('./postForwarding');
const { queuePop } = require('./deviceQueue');

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

// ==============================================================================
const deviceRoutes = async (fastify, options) => {
  const { topicName } = options;

  // H3R - File uploads are PUTs to the following routes
  fastify.put('/:type(ecgs|events|reports|logfile)/:serial/:fileName', async (request, reply) => {
    let file;
    if (request.params.type === 'ecgs') {
      file = scp;
    }
    if (request.params.type === 'events') {
      file = tze;
    }
    if (request.params.type === 'reports') {
      file = tzr;
    }
    if (request.params.type === 'logfile') {
      file = error;
    }

    // Parse the file using the correct trident-sdk parser
    const fileJson = await file.toJson(request.body);

    // Log the file for debugging/testing
    request.log.debug({ fileJson, msg: 'file received' });

    // Use google-pubsub to publish to a topic
    const { pubsub } = fastify;
    if (pubsub) {
      const attributes = {
        deviceSerial: fileJson.deviceSerial,
        type: request.params.type,
        fileName: request.params.fileName,
        uri: request.url,
      };
      await pubsub.publish(topicName, JSON.stringify(fileJson), attributes);
    }

    // Forward the parsed contents via REST API to another server
    await forwardParsedFile(fileJson, reply);

    // Construct the appropriate binary response, based on the device queue
    return queuePop(fastify, request);
  });

  // H3R - File downloads are GETs to the following routes
  fastify.get('/:type(settings|actions)/:serial', async (request, reply) =>
    // Construct the appropriate binary response, based on the device queue
    queuePop(fastify, request)
  );
};

module.exports = deviceRoutes;
