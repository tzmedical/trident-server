/* eslint-env node */

//-----------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//-----------------------------------------------------------------------------

const { URL } = require('url');
const axios = require('axios');

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

let forwardingUrlList = [];

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

// =============================================================================
const sanitizeUrlList = (urlList) => {
  // We only support HTTP/S URLs, and we want to eliminate any that have invalid characters, etc.
  const testUrl = (url) => {
    try {
      const regex = /^https?:\/\//;
      if (!regex.test(url)) {
        return false;
      }
      // eslint-disable-next-line no-new
      new URL(url);
      return true;
    } catch (err) {
      return false;
    }
  };
  // Using new Set eliminates any duplicates in the array
  return [...new Set(urlList.filter((url) => testUrl(url)))];
};

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

module.exports = {
  // ===========================================================================
  forwardParsedFile: async (fileJson, fastifyReply) => {
    if (forwardingUrlList.length) {
      let errorResponse;
      // Use axios to send the parsed contents to all configured endpoints
      try {
        await Promise.all(
          forwardingUrlList.map((url) =>
            axios({
              method: 'post',
              url,
              data: fileJson,
              headers: { 'Content-Type': 'application/json' },
              timeout: process.env.FORWARDING_TIMEOUT_MS || 30000,
            })
          )
        );
      } catch (error) {
        // If multiple responses return error statuses, the first one to be thrown will be caught here
        const source = error.config.url;
        const payload = error.response?.data || error.message;
        errorResponse = { source, error: payload };
      }
      if (errorResponse) {
        fastifyReply.code(500).type('application/json').send(errorResponse);
      }
    }
  },

  // ===========================================================================
  setForwardingUrlList: (postList) => {
    if (Array.isArray(postList) && postList.length) {
      forwardingUrlList = sanitizeUrlList(postList);
    } else if (process.env.FORWARDING_URL_LIST) {
      try {
        // If the user wants to send to multiple destinations, this should be a JSON array
        const parsedList = JSON.parse(process.env.FORWARDING_URL_LIST);
        if (Array.isArray(parsedList)) {
          forwardingUrlList = sanitizeUrlList(parsedList);
        } else {
          forwardingUrlList = [];
        }
      } catch (err) {
        // If it's not JSON-parsable, just use it as a string
        const splitString = process.env.FORWARDING_URL_LIST.split(/[,[\]]|\s+/).filter(
          (el) => el.length
        );
        forwardingUrlList = sanitizeUrlList(splitString);
      }
    }
  },
};
