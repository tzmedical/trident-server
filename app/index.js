/* eslint-env node */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

const fastify = require('fastify');
const fastifyRedis = require('@fastify/redis');
const AjvCompiler = require('@fastify/ajv-compiler');
const pubsubClient = require('fastify-google-pubsub');
const apiRoutes = require('./apiRoutes');
const deviceRoutes = require('./deviceRoutes');
const { setForwardingUrlList } = require('./postForwarding');

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

// ==============================================================================
function build(fastifyOptions = {}, applicationOptions = {}) {
  // Build the server, include custom AJV settings
  const app = fastify({
    ...fastifyOptions,
    ...{
      ajv: {
        customOptions: { discriminator: true },
      },
      schemaController: {
        compilersFactory: { buildValidator: AjvCompiler() },
      },
      pluginTimeout: 30000,
    },
  });

  const redisMaster = applicationOptions.redisMaster || process.env.REDIS_MASTER_HOST;
  const redisReplicas = applicationOptions.redisReplicas || process.env.REDIS_REPLICAS_HOST;
  const redisPassword = applicationOptions.redisPassword || process.env.REDIS_PASSWORD;

  // Add Redis database connections
  if (redisMaster) {
    app.register(fastifyRedis, {
      host: redisMaster,
      namespace: 'master',
      password: redisPassword,
    });
  }
  if (redisReplicas) {
    app.register(fastifyRedis, {
      host: redisReplicas,
      namespace: 'replicas',
      password: redisPassword,
    });
  }

  // Set up google-pubsub if enabled
  let queueTopicName;
  let actionsTopicName;
  let actionsSubscriptionName;
  if (applicationOptions.pubsub?.enabled || process.env.PUBSUB_ENABLED === 'true') {
    queueTopicName =
      applicationOptions.pubsub?.receivedTopic ||
      process.env.PUBSUB_RECEIVED_TOPIC ||
      'trident-server-file-received';
    actionsTopicName =
      applicationOptions.pubsub?.actionsTopic ||
      process.env.PUBSUB_ACTIONS_TOPIC ||
      'trident-server-actions-queue';
    actionsSubscriptionName =
      applicationOptions.pubsub?.subscription ||
      process.env.PUBSUB_SUBSCRIPTION ||
      'trident-server-actions-queue';
    const topics = [queueTopicName, actionsTopicName];
    app.register(pubsubClient, { topics });
  }

  // Add routes
  app.register(apiRoutes, {
    topicName: actionsTopicName,
    subscriptionName: actionsSubscriptionName,
  });
  app.register(deviceRoutes, {
    topicName: queueTopicName,
  });

  // Add a custom parser for application/octet-stream
  app.addContentTypeParser(
    'application/octet-stream',
    { parseAs: 'buffer' },
    (request, body, done) => {
      done(null, body);
    }
  );

  // Set up POST forwarding if the CLI flag was used
  setForwardingUrlList(applicationOptions.postList);

  return app;
}

module.exports = build;
