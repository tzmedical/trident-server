/* eslint-env node */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __           __  __  __  __
//     /   |    /\  (_  (_  |_  (_
//     \__ |__ /--\ __) __) |__ __)
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

// ==============================================================================
const tzsSchema = {
  fileSchema: {
    type: 'object',
    required: ['format', 'deviceSerial', 'deviceType', 'firmwareVersion', 'fileId', 'settings'],
    additionalProperties: false,
    properties: {
      format: {
        const: 'TZSET',
      },
      crc: {
        type: 'integer',
        maximum: 65535,
      },
      deviceType: {
        type: 'string',
        maxLength: 5,
      },
      firmwareVersion: {
        type: 'number',
        maximum: 25.5,
      },
      deviceSerial: {
        type: 'string',
        maxLength: 15,
      },
      fileId: {
        type: 'integer',
        maximum: 65535,
      },
      checkSettingRanges: {
        type: 'boolean',
      },
      settings: {
        type: 'object',
        maxProperties: 256,
      },
      patientId: {
        type: 'string',
        maxLength: 40,
      },
    },
  },
};

// ==============================================================================
const tzaSchema = {
  fileSchema: {
    type: 'object',
    required: ['format', 'deviceSerial', 'deviceType', 'firmwareVersion', 'fileId', 'actions'],
    additionalProperties: false,
    properties: {
      format: {
        const: 'TZACT',
      },
      crc: {
        type: 'integer',
        maximum: 65535,
      },
      deviceType: {
        type: 'string',
        maxLength: 5,
      },
      firmwareVersion: {
        type: 'number',
        maximum: 25.5,
      },
      deviceSerial: {
        type: 'string',
        maxLength: 15,
      },
      fileId: {
        type: 'integer',
        maximum: 65535,
      },
      actions: {
        type: 'array',
        items: {
          type: 'object',
          required: ['type', 'data'],
          properties: {
            type: {
              type: 'string',
            },
            data: {
              oneOf: [{ type: 'object', maxProperties: 10 }, { type: 'string' }],
            },
          },
        },
      },
      patientId: {
        type: 'string',
        maxLength: 40,
      },
    },
  },
};

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

module.exports = {
  tzsSchema,
  tzaSchema,
};
