/* eslint-env node */
/* eslint-disable no-unused-vars */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

const { scp, tza, tzs, tze, tzr, error } = require('@tzmedical/trident-sdk');
const Ajv = require('ajv');

const { tzsSchema, tzaSchema } = require('./fileSchemas');
const { queuePush, queueDelete } = require('./deviceQueue');

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

const pack = require('../package.json');

const queueJsonSchema = {
  type: 'object',
  discriminator: { propertyName: 'format' },
  required: ['format'],
  oneOf: [
    tzsSchema.fileSchema,
    tzaSchema.fileSchema,
    {
      type: 'object',
      required: ['format', 'deviceSerial'],
      additionalProperties: false,
      properties: {
        format: {
          const: 'DELETE',
        },
        deviceSerial: {
          type: 'string',
          maxLength: 15,
        },
      },
    },
  ],
};

const responseJsonSchema = {
  '2xx': {
    type: 'object',
    properties: {
      version: { type: 'string' },
      deviceSerial: {
        type: 'string',
        maxLength: 15,
      },
      queueCount: {
        type: 'integer',
      },
    },
  },
};

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

// ==============================================================================
function returnNotConfigured(reply) {
  // Return 501 Not Implemented if redis is not enabled
  const errorResponse = {
    statusCode: 501,
    error: 'Device queue not configured on this server',
  };
  return reply.code(501).type('application/json').send(errorResponse);
}

// ==============================================================================
async function queueItem(redis, item) {
  if (item.format === 'DELETE') {
    return queueDelete(redis, item.deviceSerial);
  }
  return queuePush(redis, item.deviceSerial, item);
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

// ==============================================================================
const apiRoutes = async (fastify, options) => {
  const { redis, pubsub, log } = fastify;
  const { topicName, subscriptionName } = options;

  // Return an empty 200 OK to the base route for health checks
  fastify.get('/', { logLevel: 'warn' }, async (request, reply) => '');

  // Report the version from the package.json
  const versionSchema = { response: responseJsonSchema };
  fastify.get('/version', { schema: versionSchema }, async (request, reply) => ({
    version: pack.version,
  }));

  fastify.post(
    '/parse/:type(ecgs|events|reports|logfile|settings|actions)',
    async (request, reply) => {
      let file;
      if (request.params.type === 'ecgs') {
        file = scp;
      }
      if (request.params.type === 'events') {
        file = tze;
      }
      if (request.params.type === 'reports') {
        file = tzr;
      }
      if (request.params.type === 'logfile') {
        file = error;
      }
      if (request.params.type === 'settings') {
        file = tzs;
      }
      if (request.params.type === 'actions') {
        file = tza;
      }

      return file.toJson(request.body);
    }
  );

  // Add an action or settings to the queue
  const queuePostSchema = { body: queueJsonSchema, response: responseJsonSchema };
  fastify.post('/queue', { schema: queuePostSchema }, async (request, reply) => {
    // Return a useful error if not configured
    if (!redis?.master) {
      return returnNotConfigured(reply);
    }

    return queueItem(redis, request.body);
  });

  // Also support google pubsub for queueing actions
  if (pubsub && redis?.master) {
    const ajv = new Ajv({ removeAdditional: true, discriminator: true });
    const validate = ajv.compile(queueJsonSchema);
    const messageHandler = async (message) => {
      log.debug('Received message:', message.data);
      const jsonPayload = JSON.parse(message.data);

      // Queue item only if it is valid
      if (validate(jsonPayload)) {
        await queueItem(redis, jsonPayload);
      }
    };
    await pubsub.subscribe(topicName, subscriptionName, messageHandler);
  }
};

module.exports = apiRoutes;
