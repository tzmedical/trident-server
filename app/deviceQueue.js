/* eslint-env node */

//-----------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//-----------------------------------------------------------------------------

const { utils } = require('@tzmedical/trident-sdk');

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

// ==============================================================================
function getQueueKey(deviceSerial) {
  return `${deviceSerial}#queue`;
}

// ==============================================================================
function getCacheKey(deviceSerial) {
  return `${deviceSerial}#cache`;
}

// ==============================================================================
function detectEnrollmentMismatch(queueEnrollmentId, patientId) {
  if (queueEnrollmentId && queueEnrollmentId?.length > 3 && queueEnrollmentId !== patientId) {
    return true;
  }
  return false;
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

module.exports = {
  // ===========================================================================
  queuePop: async function queuePop(fastify, request) {
    // Return an empty body if there is no Redis database to read from
    const { redis } = fastify;
    if (!redis?.replicas) {
      if (redis?.master) {
        redis.replicas = redis.master;
      } else {
        return Buffer.alloc(0);
      }
    }

    // Extract what we need from the request
    const deviceSerial = request.params.serial;
    const fileName = request.params.fileName || request.params.type;
    const patientId = request.headers['tzmedical-patient-id'];

    // Return the cached if the fileName matches the hash
    const cacheKey = getCacheKey(deviceSerial);
    const cacheFileName = await redis.replicas.hget(cacheKey, 'fileName');
    if (fileName === cacheFileName) {
      return redis.replicas.hgetBuffer(cacheKey, 'response');
    }

    // Get the next item from the queue. The do...while is there because we only send
    // items that have a blank/null patientId or that have a value that matches
    // the value from the HTTP request header
    const queueKey = getQueueKey(deviceSerial);
    let queueJson = {};
    do {
      // eslint-disable-next-line no-await-in-loop
      const queueString = await redis.master.rpop(queueKey);
      if (!queueString || !queueString.length || queueString.length < 10) {
        queueJson = {};
      } else {
        queueJson = JSON.parse(queueString);
      }

      if (detectEnrollmentMismatch(queueJson.patientId, patientId)) {
        request.log.info({
          patientId,
          queueEnrollmentId: queueJson.patientId,
          fileId: queueJson.fileId,
          msg: 'discarding queue item',
        });
      } else if (queueJson.fileId) {
        request.log.info({
          fileId: queueJson.fileId,
          msg: 'popped queue item',
        });
      }
    } while (detectEnrollmentMismatch(queueJson.patientId, patientId));

    // Log the item to be created for debugging/testing
    request.log.debug({ queueJson, msg: 'creating response' });

    // Construct the device binary file from the JSON
    const response = queueJson.format ? await utils.fromJson(queueJson) : Buffer.alloc(0);

    // Store the fileName and the Buffer in case the device does a retry
    await redis.master.hset(cacheKey, 'fileName', fileName, 'response', response);

    // Return the response as a Buffer once it has been cached
    return response;
  },

  // ===========================================================================
  queuePush: async function queuePush(redis, deviceSerial, item) {
    // Add an item to the queue
    const key = getQueueKey(deviceSerial);
    const queueCount = await redis.master.rpush(key, JSON.stringify(item));
    return { deviceSerial, queueCount };
  },

  // ===========================================================================
  queueDelete: async function queueDelete(redis, deviceSerial) {
    // Remove ALL actions or settings for the queue
    const queueKey = getQueueKey(deviceSerial);
    await redis.master.ltrim(queueKey, 1, 0);
    const cacheKey = getCacheKey(deviceSerial);
    await redis.master.hset(cacheKey, 'fileName', -1);
    return { deviceSerial, queueCount: 0 };
  },
};
