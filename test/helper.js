/* eslint-env node */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

const tap = require('tap');
const pack = require('../package.json');

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

// Set this to true to use `tap.only` to focus a test
tap.runOnly = false;

const deviceUserAgent = 'H3R/v2.1/g1.8';

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

// ==============================================================================
const doRequestAndCheckResponse = async ({
  t,
  app,
  file,
  deviceSerial,
  patientId,
  expectedStatus,
  expectedResponse,
  parser,
}) => {
  const expectedHttpStatus = expectedStatus || 200;
  const fileName = file.name ? `/${file.name}` : '';
  const response = await app.inject({
    method: `${file.method}`,
    url: `/${file.type}/${deviceSerial}${fileName}`,
    headers: {
      'user-agent': deviceUserAgent,
      'tzmedical-device-serial': deviceSerial,
      'tzmedical-patient-id': patientId,
      'content-type': 'application/octet-stream',
    },
    payload: file.payload,
  });
  t.equal(
    response.statusCode,
    expectedHttpStatus,
    `${file.method} /${file.type} should return a status code of 200`
  );

  if (response.statusCode !== expectedHttpStatus && expectedHttpStatus === 200) {
    // eslint-disable-next-line no-console
    console.log(response.body);
  }

  if (expectedResponse.format && parser) {
    t.equal(
      response.headers['content-type'],
      'application/octet-stream',
      `${file.method} /${file.type} should return content-type: application/octet-stream`
    );
    const parsedResponse = await parser.toJson(response.rawPayload);
    // Remove the patientId from the expected response
    // eslint-disable-next-line no-param-reassign
    delete expectedResponse.patientId;
    t.same(parsedResponse, expectedResponse, `GET /${file.type} should return the queued content`);
  } else if (expectedResponse.length) {
    t.equal(
      response.body,
      expectedResponse,
      `${file.method} /${file.type} should return a useful response`
    );
  } else {
    t.equal(
      response.headers['content-type'],
      'application/octet-stream',
      `${file.method} /${file.type} should return content-type: application/octet-stream`
    );
    t.equal(response.body, '', `${file.method} /${file.type} should return an empty body`);
  }
};

// ==============================================================================
const sleep = (ms) =>
  new Promise((r) => {
    setTimeout(r, ms);
  });

module.exports = {
  doRequestAndCheckResponse,
  sleep,
  deviceUserAgent,
  packageVersion: pack.version,
};
