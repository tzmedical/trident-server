/* eslint-env node */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

const tap = require('tap');
const fs = require('fs');
const nock = require('nock');
const buildFastify = require('../app');
const { doRequestAndCheckResponse } = require('./helper');

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

const testEcgJson = require('./testEcg.json');

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

// ==============================================================================
tap.test('Single end-point POST forwarding', async (t) => {
  const opts = {};
  const postHost = 'http://single.test';
  const postEndpoint = '/endpoint';
  const app = buildFastify(opts, { postList: [postHost + postEndpoint] });

  // This test has 5 expects
  t.plan(5);

  const testPayload = fs.readFileSync(`${__dirname}/testEcg.scp`);
  const { deviceSerial } = testEcgJson;
  const { sequence } = testEcgJson;
  const fileName = `${deviceSerial}_${sequence}.scp`;

  // Nock the forwarding route
  const scope = nock(postHost)
    .post(postEndpoint, (body) => t.same(body, testEcgJson, 'should forward the parsed JSON'))
    .reply(200, '');

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => {
    app.close();
    scope.remove();
  });

  // Run the test
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'ecgs',
      name: fileName,
      payload: testPayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: {},
  });
  t.equal(scope.isDone(), true, 'should forward to the specified destination');
});

// ==============================================================================
tap.test('Single end-point POST forwarding Environment Variable Configuration', async (t) => {
  const opts = {};
  const postHost = 'http://single.test';
  const postEndpoint = '/endpointENV';
  process.env.FORWARDING_URL_LIST = postHost + postEndpoint;
  const app = buildFastify(opts);

  // This test has 5 expects
  t.plan(5);

  const testPayload = fs.readFileSync(`${__dirname}/testEcg.scp`);
  const { deviceSerial } = testEcgJson;
  const { sequence } = testEcgJson;
  const fileName = `${deviceSerial}_${sequence}.scp`;

  // Nock the forwarding route
  const scope = nock(postHost)
    .post(postEndpoint, (body) => t.same(body, testEcgJson, 'should forward the parsed JSON'))
    .reply(200, '');

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => {
    app.close();
    scope.remove();
    process.env.FORWARDING_URL_LIST = '';
  });

  // Run the test
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'ecgs',
      name: fileName,
      payload: testPayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: {},
  });
  t.equal(scope.isDone(), true, 'should forward to the specified destination');
});

// ==============================================================================
tap.test(
  'Single end-point POST forwarding invalid Environment Variable Configuration',
  async (t) => {
    const opts = {};
    const postHost = 'http://single.test';
    const postEndpoint = '/invalid';
    process.env.FORWARDING_URL_LIST = `{"server": "${postHost}${postEndpoint}"}`;
    const app = buildFastify(opts);

    // This test has 4 expects
    t.plan(4);

    const testPayload = fs.readFileSync(`${__dirname}/testEcg.scp`);
    const { deviceSerial } = testEcgJson;
    const { sequence } = testEcgJson;
    const fileName = `${deviceSerial}_${sequence}.scp`;

    // Nock the forwarding route
    const scope = nock(postHost).post(postEndpoint).reply(200, '');

    // At the end of your tests it is highly recommended to call `.close()`
    // to ensure that all connections to external services get closed.
    t.teardown(() => {
      app.close();
      scope.remove();
      process.env.FORWARDING_URL_LIST = '';
    });

    // Run the test
    await doRequestAndCheckResponse({
      t,
      app,
      file: {
        method: 'PUT',
        type: 'ecgs',
        name: fileName,
        payload: testPayload,
      },
      deviceSerial,
      patientId: '',
      expectedResponse: {},
    });
    t.equal(scope.isDone(), false, 'should not forward if given an invalid environment variable');
  }
);

// ==============================================================================
tap.test('Single end-point POST forwarding server 404 response', async (t) => {
  const opts = {};
  const postHost = 'http://single.test';
  const postEndpoint = '/endpoint404';
  const app = buildFastify(opts, { postList: [postHost + postEndpoint] });

  // This test has 4 expects
  t.plan(4);

  const testPayload = fs.readFileSync(`${__dirname}/testEcg.scp`);
  const { deviceSerial } = testEcgJson;
  const { sequence } = testEcgJson;
  const fileName = `${deviceSerial}_${sequence}.scp`;

  const serverErrorResponse = 'An informative error message';

  // Nock the forwarding route
  const scope = nock(postHost)
    .post(postEndpoint, (body) => t.same(body, testEcgJson, 'should forward the parsed JSON'))
    .reply(404, serverErrorResponse);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => {
    app.close();
    scope.remove();
  });

  // Run the test
  const expected = { source: postHost + postEndpoint, error: serverErrorResponse };
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'ecgs',
      name: fileName,
      payload: testPayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: JSON.stringify(expected),
    expectedStatus: 500,
  });
  t.equal(scope.isDone(), true, 'should forward to the specified destination');
});

// ==============================================================================
tap.test('Single end-point POST forwarding bad configuration', async (t) => {
  const opts = {};
  const postHost = 'http://never-resolve';
  const postEndpoint = '/endpoint';
  const app = buildFastify(opts, { postList: [postHost + postEndpoint] });
  // Set the connection timeout really low
  process.env.FORWARDING_TIMEOUT_MS = '100';

  // This test has 4 expects
  t.plan(4);

  const testPayload = fs.readFileSync(`${__dirname}/testEcg.scp`);
  const { deviceSerial } = testEcgJson;
  const { sequence } = testEcgJson;
  const fileName = `${deviceSerial}_${sequence}.scp`;

  const serverErrorResponse = 'timeout of 100ms exceeded';

  // Nock the forwarding route
  const scope = nock(postHost)
    .post(postEndpoint, (body) => t.same(body, testEcgJson, 'should forward the parsed JSON'))
    .delayConnection(200) // 0.2 seconds
    .reply(200, '');

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => {
    app.close();
    scope.remove();
    process.env.FORWARDING_TIMEOUT_MS = '';
  });

  // Run the test
  const expected = { source: postHost + postEndpoint, error: serverErrorResponse };
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'ecgs',
      name: fileName,
      payload: testPayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: JSON.stringify(expected),
    expectedStatus: 500,
  });
  t.equal(scope.isDone(), true, 'should forward to the specified destination');
});

// ==============================================================================
tap.test('Double end-point POST forwarding', async (t) => {
  const opts = {};
  const postHost1 = 'http://double.one.test';
  const postEndpoint1 = '/endpoint';
  const postHost2 = 'http://double.two.test';
  const postEndpoint2 = '/otherEndpoint';
  const app = buildFastify(opts, {
    postList: [postHost1 + postEndpoint1, postHost2 + postEndpoint2],
  });

  // This test has 7 expects
  t.plan(7);

  const testPayload = fs.readFileSync(`${__dirname}/testEcg.scp`);
  const { deviceSerial } = testEcgJson;
  const { sequence } = testEcgJson;
  const fileName = `${deviceSerial}_${sequence}.scp`;

  // Nock the forwarding routes
  const scope1 = nock(postHost1)
    .post(postEndpoint1, (body) =>
      t.same(body, testEcgJson, 'should forward the parsed JSON to endpoint 1')
    )
    .reply(200, '');
  const scope2 = nock(postHost2)
    .post(postEndpoint2, (body) =>
      t.same(body, testEcgJson, 'should forward the parsed JSON to endpoint 2')
    )
    .reply(200, '');

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => {
    app.close();
    scope1.remove();
    scope2.remove();
  });

  // Run the test
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'ecgs',
      name: fileName,
      payload: testPayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: {},
  });
  t.equal(scope1.isDone(), true, 'should forward to the first specified destination');
  t.equal(scope2.isDone(), true, 'should forward to the second specified destination');
});

// ==============================================================================
tap.test('Double end-point POST forwarding invalid URL configuration', async (t) => {
  const opts = {};
  const postHost1 = 'ftp://double.one.test';
  const postEndpoint1 = '/invalidProtocol';
  const postHost2 = 'http://double test';
  const postEndpoint2 = '/invalidStructure';
  const app = buildFastify(opts, {
    postList: [postHost1 + postEndpoint1, postHost2 + postEndpoint2],
  });

  // This test has 3 expects
  t.plan(3);

  const testPayload = fs.readFileSync(`${__dirname}/testEcg.scp`);
  const { deviceSerial } = testEcgJson;
  const { sequence } = testEcgJson;
  const fileName = `${deviceSerial}_${sequence}.scp`;

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => {
    app.close();
  });

  // Run the test
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'ecgs',
      name: fileName,
      payload: testPayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: {},
  });
});

// ==============================================================================
tap.test(
  'Double end-point POST forwarding Environment Variable Configuration, Array',
  async (t) => {
    const opts = {};
    const postHost1 = 'http://double.one.test';
    const postEndpoint1 = '/endpointArray';
    const postHost2 = 'http://double.two.test';
    const postEndpoint2 = '/otherEndpointArray';
    process.env.FORWARDING_URL_LIST = `["${postHost1}${postEndpoint1}","${postHost2}${postEndpoint2}"]`;
    const app = buildFastify(opts, { postList: [] });

    // This test has 7 expects
    t.plan(7);

    const testPayload = fs.readFileSync(`${__dirname}/testEcg.scp`);
    const { deviceSerial } = testEcgJson;
    const { sequence } = testEcgJson;
    const fileName = `${deviceSerial}_${sequence}.scp`;

    // Nock the forwarding routes
    const scope1 = nock(postHost1)
      .post(postEndpoint1, (body) =>
        t.same(body, testEcgJson, 'should forward the parsed JSON to endpoint 1')
      )
      .reply(200, '');
    const scope2 = nock(postHost2)
      .post(postEndpoint2, (body) =>
        t.same(body, testEcgJson, 'should forward the parsed JSON to endpoint 2')
      )
      .reply(200, '');

    // At the end of your tests it is highly recommended to call `.close()`
    // to ensure that all connections to external services get closed.
    t.teardown(() => {
      app.close();
      scope1.remove();
      scope2.remove();
      process.env.FORWARDING_URL_LIST = '';
    });

    // Run the test
    await doRequestAndCheckResponse({
      t,
      app,
      file: {
        method: 'PUT',
        type: 'ecgs',
        name: fileName,
        payload: testPayload,
      },
      deviceSerial,
      patientId: '',
      expectedResponse: {},
    });
    t.equal(scope1.isDone(), true, 'should forward to the first specified destination');
    t.equal(scope2.isDone(), true, 'should forward to the second specified destination');
  }
);

// ==============================================================================
tap.test(
  'Double end-point POST forwarding Environment Variable Configuration, Spaced Array',
  async (t) => {
    const opts = {};
    const postHost1 = 'http://double.one.test';
    const postEndpoint1 = '/endpointArray';
    const postHost2 = 'http://double.two.test';
    const postEndpoint2 = '/otherEndpointArray';
    process.env.FORWARDING_URL_LIST = `[${postHost1}${postEndpoint1} ${postHost2}${postEndpoint2}]`;
    const app = buildFastify(opts, { postList: [] });

    // This test has 7 expects
    t.plan(7);

    const testPayload = fs.readFileSync(`${__dirname}/testEcg.scp`);
    const { deviceSerial } = testEcgJson;
    const { sequence } = testEcgJson;
    const fileName = `${deviceSerial}_${sequence}.scp`;

    // Nock the forwarding routes
    const scope1 = nock(postHost1)
      .post(postEndpoint1, (body) =>
        t.same(body, testEcgJson, 'should forward the parsed JSON to endpoint 1')
      )
      .reply(200, '');
    const scope2 = nock(postHost2)
      .post(postEndpoint2, (body) =>
        t.same(body, testEcgJson, 'should forward the parsed JSON to endpoint 2')
      )
      .reply(200, '');

    // At the end of your tests it is highly recommended to call `.close()`
    // to ensure that all connections to external services get closed.
    t.teardown(() => {
      app.close();
      scope1.remove();
      scope2.remove();
      process.env.FORWARDING_URL_LIST = '';
    });

    // Run the test
    await doRequestAndCheckResponse({
      t,
      app,
      file: {
        method: 'PUT',
        type: 'ecgs',
        name: fileName,
        payload: testPayload,
      },
      deviceSerial,
      patientId: '',
      expectedResponse: {},
    });
    t.equal(scope1.isDone(), true, 'should forward to the first specified destination');
    t.equal(scope2.isDone(), true, 'should forward to the second specified destination');
  }
);

// ==============================================================================
tap.test(
  'Double end-point POST forwarding Environment Variable Configuration, Commas',
  async (t) => {
    const opts = {};
    const postHost1 = 'http://double.one.test';
    const postEndpoint1 = '/endpointComma';
    const postHost2 = 'http://double.two.test';
    const postEndpoint2 = '/otherEndpointComma';
    process.env.FORWARDING_URL_LIST = `${postHost1}${postEndpoint1},${postHost2}${postEndpoint2}`;
    const app = buildFastify(opts, { postList: [] });

    // This test has 7 expects
    t.plan(7);

    const testPayload = fs.readFileSync(`${__dirname}/testEcg.scp`);
    const { deviceSerial } = testEcgJson;
    const { sequence } = testEcgJson;
    const fileName = `${deviceSerial}_${sequence}.scp`;

    // Nock the forwarding routes
    const scope1 = nock(postHost1)
      .post(postEndpoint1, (body) =>
        t.same(body, testEcgJson, 'should forward the parsed JSON to endpoint 1')
      )
      .reply(200, '');
    const scope2 = nock(postHost2)
      .post(postEndpoint2, (body) =>
        t.same(body, testEcgJson, 'should forward the parsed JSON to endpoint 2')
      )
      .reply(200, '');

    // At the end of your tests it is highly recommended to call `.close()`
    // to ensure that all connections to external services get closed.
    t.teardown(() => {
      app.close();
      scope1.remove();
      scope2.remove();
      process.env.FORWARDING_URL_LIST = '';
    });

    // Run the test
    await doRequestAndCheckResponse({
      t,
      app,
      file: {
        method: 'PUT',
        type: 'ecgs',
        name: fileName,
        payload: testPayload,
      },
      deviceSerial,
      patientId: '',
      expectedResponse: {},
    });
    t.equal(scope1.isDone(), true, 'should forward to the first specified destination');
    t.equal(scope2.isDone(), true, 'should forward to the second specified destination');
  }
);

// ==============================================================================
tap.test('Double end-point POST forwarding 500 response', async (t) => {
  const opts = {};
  const postHost1 = 'http://double.one.test';
  const postEndpoint1 = '/endpoint200';
  const postHost2 = 'http://double.two.test';
  const postEndpoint2 = '/otherEndpoint404';
  const app = buildFastify(opts, {
    postList: [postHost1 + postEndpoint1, postHost2 + postEndpoint2],
  });

  // This test has 6 expects
  t.plan(6);

  const testPayload = fs.readFileSync(`${__dirname}/testEcg.scp`);
  const { deviceSerial } = testEcgJson;
  const { sequence } = testEcgJson;
  const fileName = `${deviceSerial}_${sequence}.scp`;

  const serverErrorResponse2 = 'A useful error response';

  // Nock the forwarding routes
  const scope1 = nock(postHost1)
    .post(postEndpoint1, (body) =>
      t.same(body, testEcgJson, 'should forward the parsed JSON to endpoint 1')
    )
    .reply(200, '');
  const scope2 = nock(postHost2)
    .post(postEndpoint2, (body) =>
      t.same(body, testEcgJson, 'should forward the parsed JSON to endpoint 2')
    )
    .reply(404, serverErrorResponse2);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => {
    app.close();
    scope1.remove();
    scope2.remove();
  });

  // Run the test
  const expected = { source: postHost2 + postEndpoint2, error: serverErrorResponse2 };
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'ecgs',
      name: fileName,
      payload: testPayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: JSON.stringify(expected),
    expectedStatus: 500,
  });
  t.equal(scope1.isDone(), true, 'should forward to the first specified destination');
  t.equal(scope2.isDone(), true, 'should forward to the second specified destination');
});
