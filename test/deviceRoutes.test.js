/* eslint-env node */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

const tap = require('tap');
const fs = require('fs');
const sinon = require('sinon');
const buildFastify = require('../app');
const { doRequestAndCheckResponse } = require('./helper');

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

const testErrorJson = require('./testError.json');
const testEcgJson = require('./testEcg.json');
const testEventJson = require('./testEvent.json');
const testReportJson = require('./testReport.json');

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

// ==============================================================================
tap.test('GET requests to the "/actions" route', async (t) => {
  const app = buildFastify();

  // This test has 3 expects
  t.plan(3);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => app.close());

  const deviceSerial = 'H3R3001234';
  await doRequestAndCheckResponse({
    t,
    app,
    file: { method: 'GET', type: 'actions' },
    deviceSerial,
    patientId: '',
    expectedResponse: {},
  });
});

// ==============================================================================
tap.test('GET requests to the "/settings" route', async (t) => {
  const app = buildFastify();

  // This test has 3 expects
  t.plan(3);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => app.close());

  const deviceSerial = 'H3R3001234';
  await doRequestAndCheckResponse({
    t,
    app,
    file: { method: 'GET', type: 'settings' },
    deviceSerial,
    patientId: '',
    expectedResponse: {},
  });
});

// ==============================================================================
tap.test('PUT requests to the "/ecgs" route', async (t) => {
  const app = buildFastify();

  // This test has 5 expects
  t.plan(5);

  const testPayload = fs.readFileSync(`${__dirname}/testEcg.scp`);
  const { deviceSerial } = testEcgJson;
  const { sequence } = testEcgJson;
  const fileName = `${deviceSerial}_${sequence}.scp`;

  // Stub the logger to check for the parsed contents
  const stub = sinon.stub(app.log, 'debug').callsFake((message) => {
    t.same(message.fileJson, testEcgJson, 'should parse the file correctly');
  });

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => {
    stub.restore();
    app.close();
  });

  // Run the test
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'ecgs',
      name: fileName,
      payload: testPayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: {},
  });
  t.equal(stub.called, true, 'should log the parsed file');
});

// ==============================================================================
tap.test('PUT requests to the "/events" route', async (t) => {
  const app = buildFastify();

  // This test has 5 expects
  t.plan(5);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => app.close());

  const testPayload = fs.readFileSync(`${__dirname}/testEvent.tze`);
  const { deviceSerial } = testEventJson;
  const { sequence } = testEventJson;
  const count = '001';
  const fileName = `${deviceSerial}_${sequence}_${count}.tze`;

  // Stub the logger to check for the parsed contents
  const stub = sinon.stub(app.log, 'debug').callsFake((message) => {
    t.same(message.fileJson, testEventJson, 'should parse the file correctly');
  });

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => {
    stub.restore();
    app.close();
  });

  // Run the test
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'events',
      name: fileName,
      payload: testPayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: {},
  });
  t.equal(stub.called, true, 'should log the parsed file');
});

// ==============================================================================
tap.test('PUT requests to the "/reports" route', async (t) => {
  const app = buildFastify();

  // This test has 5 expects
  t.plan(5);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => app.close());

  const testPayload = fs.readFileSync(`${__dirname}/testReport.tzr`);
  const { deviceSerial } = testReportJson;
  const dateTime = testReportJson.events[0].datetime
    .substring(0, 13)
    .replace(/-/g, '')
    .replace(/ /g, '_');
  const fileName = `${deviceSerial}_${dateTime}.tzr`;

  // Stub the logger to check for the parsed contents
  const stub = sinon.stub(app.log, 'debug').callsFake((message) => {
    t.same(message.fileJson, testReportJson, 'should parse the file correctly');
  });

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => {
    stub.restore();
    app.close();
  });

  // Run the test
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'reports',
      name: fileName,
      payload: testPayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: {},
  });
  t.equal(stub.called, true, 'should log the parsed file');
});

// ==============================================================================
tap.test('PUT requests to the "/logfile" route', async (t) => {
  const app = buildFastify();

  // This test has 5 expects
  t.plan(5);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => app.close());

  const testPayload = fs.readFileSync(`${__dirname}/testError.log`);
  const deviceSerial = 'H3R3001347';
  const fileName = `${deviceSerial}_error.log`;

  // Stub the logger to check for the parsed contents
  const stub = sinon.stub(app.log, 'debug').callsFake((message) => {
    t.same(message.fileJson, testErrorJson, 'should parse the file correctly');
  });

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => {
    stub.restore();
    app.close();
  });

  // Run the test
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'logfile',
      name: fileName,
      payload: testPayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: {},
  });
  t.equal(stub.called, true, 'should log the parsed file');
});
