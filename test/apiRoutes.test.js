/* eslint-env node */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

const tap = require('tap');
const fs = require('fs');

const buildFastify = require('../app');
const { packageVersion } = require('./helper');

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

const testEcgJson = require('./testEcg.json');
const testEventJson = require('./testEvent.json');
const testReportJson = require('./testReport.json');
const testErrorJson = require('./testError.json');
const testActionsJson = require('./testActions.json');
const testSettingsJson = require('./testSettings.json');

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

// ==============================================================================
tap.test('GET requests to the "/" route', async (t) => {
  const app = buildFastify();

  // This test has 2 expects
  t.plan(2);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => app.close());

  const response = await app.inject({
    method: 'GET',
    url: '/',
  });
  t.equal(response.statusCode, 200, 'should return a status code of 200');
  t.equal(response.body, '', 'should return an empty body');
});

// ==============================================================================
tap.test('GET requests to the "/version" route', async (t) => {
  const app = buildFastify();

  // This test has 2 expects
  t.plan(2);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => app.close());

  const response = await app.inject({
    method: 'GET',
    url: '/version',
  });
  t.equal(response.statusCode, 200, 'should return a status code of 200');
  const expected = JSON.stringify({ version: packageVersion });
  t.equal(response.body, expected, 'should return the version of the system');
});

// ==============================================================================
tap.test('sending SCP files to the "POST /parse/ecgs" route', async (t) => {
  const app = buildFastify();

  // This test has 2 expects
  t.plan(2);

  const testPayload = fs.readFileSync(`${__dirname}/testEcg.scp`);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => app.close());

  const response = await app.inject({
    method: 'POST',
    url: '/parse/ecgs',
    payload: testPayload,
    headers: {
      'content-type': 'application/octet-stream',
    },
  });
  t.equal(response.statusCode, 200, 'should return a status code of 200');
  t.equal(response.body, JSON.stringify(testEcgJson), 'should return the parsed file');
});

// ==============================================================================
tap.test('sending TZE files to the "POST /parse/events" route', async (t) => {
  const app = buildFastify();

  // This test has 2 expects
  t.plan(2);

  const testPayload = fs.readFileSync(`${__dirname}/testEvent.tze`);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => app.close());

  const response = await app.inject({
    method: 'POST',
    url: '/parse/events',
    payload: testPayload,
    headers: {
      'content-type': 'application/octet-stream',
    },
  });
  t.equal(response.statusCode, 200, 'should return a status code of 200');
  t.equal(response.body, JSON.stringify(testEventJson), 'should return the parsed file');
});

// ==============================================================================
tap.test('sending TZR files to the "POST /parse/reports" route', async (t) => {
  const app = buildFastify();

  // This test has 2 expects
  t.plan(2);

  const testPayload = fs.readFileSync(`${__dirname}/testReport.tzr`);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => app.close());

  const response = await app.inject({
    method: 'POST',
    url: '/parse/reports',
    payload: testPayload,
    headers: {
      'content-type': 'application/octet-stream',
    },
  });
  t.equal(response.statusCode, 200, 'should return a status code of 200');
  t.equal(response.body, JSON.stringify(testReportJson), 'should return the parsed file');
});

// ==============================================================================
tap.test('sending error log files to the "POST /parse/logfile" route', async (t) => {
  const app = buildFastify();

  // This test has 2 expects
  t.plan(2);

  const testPayload = fs.readFileSync(`${__dirname}/testError.log`);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => app.close());

  const response = await app.inject({
    method: 'POST',
    url: '/parse/logfile',
    payload: testPayload,
    headers: {
      'content-type': 'application/octet-stream',
    },
  });
  t.equal(response.statusCode, 200, 'should return a status code of 200');
  t.equal(response.body, JSON.stringify(testErrorJson), 'should return the parsed file');
});

// ==============================================================================
tap.test('sending TZA files to the "POST /parse/actions" route', async (t) => {
  const app = buildFastify();

  // This test has 2 expects
  t.plan(2);

  const testPayload = fs.readFileSync(`${__dirname}/testActions.tza`);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => app.close());

  const response = await app.inject({
    method: 'POST',
    url: '/parse/actions',
    payload: testPayload,
    headers: {
      'content-type': 'application/octet-stream',
    },
  });
  t.equal(response.statusCode, 200, 'should return a status code of 200');
  t.equal(response.body, JSON.stringify(testActionsJson), 'should return the parsed file');
});

// ==============================================================================
tap.test('sending TZS files to the "POST /parse/settings" route', async (t) => {
  const app = buildFastify();

  // This test has 2 expects
  t.plan(2);

  const testPayload = fs.readFileSync(`${__dirname}/testSettings.tzs`);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => app.close());

  const response = await app.inject({
    method: 'POST',
    url: '/parse/settings',
    payload: testPayload,
    headers: {
      'content-type': 'application/octet-stream',
    },
  });
  t.equal(response.statusCode, 200, 'should return a status code of 200');
  t.equal(response.body, JSON.stringify(testSettingsJson), 'should return the parsed file');
});

// ==============================================================================
tap.test('creation requests to the "POST /queue" route, when not configured', async (t) => {
  const app = buildFastify();

  // This test has 2 expects
  t.plan(2);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => app.close());

  const response = await app.inject({
    method: 'POST',
    url: '/queue',
    payload: testActionsJson,
  });
  t.equal(response.statusCode, 501, 'should return a status code of 501');
  const expected = JSON.stringify({
    statusCode: 501,
    error: 'Device queue not configured on this server',
  });
  t.equal(response.body, expected, 'should return an error message');
});

// ==============================================================================
tap.test('invalid creation requests to the "POST /queue" route', async (t) => {
  const app = buildFastify({}, { redisMaster: 'localhost' });

  // This test has 2 expects
  t.plan(2);

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(() => {
    app.close();
  });

  const response = await app.inject({
    method: 'POST',
    url: '/queue',
    payload: testEventJson,
  });
  t.equal(response.statusCode, 400, 'should return a status code of 400');
  const expected = JSON.stringify({
    statusCode: 400,
    code: 'FST_ERR_VALIDATION',
    error: 'Bad Request',
    message: 'body value of tag "format" must be in oneOf',
  });
  t.equal(response.body, expected, 'should return an error message regarding the body format');
});

// ==============================================================================
tap.test('valid creation requests to the "POST /queue" routes', async (t) => {
  // NOTE: This test requires a running redis instance at localhost. See the readme
  // for some options to set that up.
  const app = buildFastify({}, { redisMaster: 'localhost' });

  // This test has 4 expects
  t.plan(4);

  const deletePayload = {
    format: 'DELETE',
    deviceSerial: testActionsJson.deviceSerial,
  };

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(async () => {
    // A 'backup' delete all, in case the first expects fail...
    await app.inject({
      method: 'POST',
      url: '/queue',
      payload: deletePayload,
    });
    app.close();
  });

  await app.inject({
    method: 'POST',
    url: '/queue',
    payload: deletePayload,
  });
  const response = await app.inject({
    method: 'POST',
    url: '/queue',
    payload: testActionsJson,
  });
  t.equal(response.statusCode, 200, 'should return a status code of 200');
  const queueExpected = JSON.stringify({
    deviceSerial: testActionsJson.deviceSerial,
    queueCount: 1,
  });
  t.equal(response.body, queueExpected, 'should return the number of queued items for the device');
  const deleteResponse = await app.inject({
    method: 'POST',
    url: '/queue',
    payload: deletePayload,
  });
  t.equal(deleteResponse.statusCode, 200, 'should return a status code of 200');
  const deleteExpected = JSON.stringify({
    deviceSerial: testActionsJson.deviceSerial,
    queueCount: 0,
  });
  t.equal(
    deleteResponse.body,
    deleteExpected,
    'should return the number of queued items for the device'
  );
});

// ==============================================================================
tap.test(
  'valid creation requests to the "POST /queue" routes, Environment Variable Configuration',
  async (t) => {
    // NOTE: This test requires a running redis instance at localhost. See the readme
    // for some options to set that up.
    process.env.REDIS_MASTER_HOST = 'localhost';
    const app = buildFastify();

    // This test has 4 expects
    t.plan(4);

    const deletePayload = {
      format: 'DELETE',
      deviceSerial: testActionsJson.deviceSerial,
    };

    // At the end of your tests it is highly recommended to call `.close()`
    // to ensure that all connections to external services get closed.
    t.teardown(async () => {
      // A 'backup' delete all, in case the first expects fail...
      await app.inject({
        method: 'POST',
        url: '/queue',
        payload: deletePayload,
      });
      app.close();
      delete process.env.REDIS_MASTER_HOST;
    });

    await app.inject({
      method: 'POST',
      url: '/queue',
      payload: deletePayload,
    });
    const response = await app.inject({
      method: 'POST',
      url: '/queue',
      payload: testActionsJson,
    });
    t.equal(response.statusCode, 200, 'should return a status code of 200');
    const queueExpected = JSON.stringify({
      deviceSerial: testActionsJson.deviceSerial,
      queueCount: 1,
    });
    t.equal(
      response.body,
      queueExpected,
      'should return the number of queued items for the device'
    );
    const deleteResponse = await app.inject({
      method: 'POST',
      url: '/queue',
      payload: deletePayload,
    });
    t.equal(deleteResponse.statusCode, 200, 'should return a status code of 200');
    const deleteExpected = JSON.stringify({
      deviceSerial: testActionsJson.deviceSerial,
      queueCount: 0,
    });
    t.equal(
      deleteResponse.body,
      deleteExpected,
      'should return the number of queued items for the device'
    );
  }
);
