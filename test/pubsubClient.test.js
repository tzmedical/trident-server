/* eslint-env node */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

const tap = require('tap');
const Emulator = require('google-pubsub-emulator');
const EventEmitter = require('events');

const fs = require('fs');
const { tza } = require('@tzmedical/trident-sdk');
const { doRequestAndCheckResponse, sleep } = require('./helper');
const buildFastify = require('../app');

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

const testEcgJson = require('./testEcg.json');
const testEventJson = require('./testEvent.json');
const testActionsJson = require('./testActions.json');

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

tap.test('pubsubClient', async (d) => {
  process.env.GCLOUD_PROJECT = 'test-project-id'; // Set the gcloud project Id globally
  let emulator;

  // ==============================================================================
  d.before(() => {
    emulator = new Emulator({ port: 30000 });

    return emulator.start();
  });

  // ==============================================================================
  d.teardown(() => emulator.stop());

  // ==============================================================================
  d.test('Publishing received files', async (t) => {
    const opts = {};
    const topicName = 'test-file-received';
    const pubsubOptions = {
      enabled: true,
      receivedTopic: topicName,
    };
    const app = buildFastify(opts, { pubsub: pubsubOptions });

    // This test has 5 expects
    t.plan(5);

    // At the end of your tests it is highly recommended to call `.close()`
    // to ensure that all connections to external services get closed.
    t.teardown(() => {
      app.close();
    });

    const testPayload = fs.readFileSync(`${__dirname}/testEcg.scp`);
    const { deviceSerial } = testEcgJson;
    const { sequence } = testEcgJson;
    const fileName = `${deviceSerial}_${sequence}.scp`;

    // Subscribe to the test topic
    const eventEmitter = new EventEmitter();
    const messageHandler = (message) => {
      const body = JSON.parse(message.data.toString());
      t.same(body, testEcgJson, 'should publish the parsed JSON');
      const testAttributes = {
        deviceSerial,
        type: 'ecgs',
        fileName,
        uri: `/ecgs/${deviceSerial}/${fileName}`,
      };
      t.same(
        testAttributes,
        message.attributes,
        'published message has attributes of serial number, type, and filename'
      );
      eventEmitter.emit('message');
    };
    const messagePromise = new Promise((resolve) => {
      eventEmitter.on('message', resolve);
    });
    await app.ready(); // <- this makes sure the pubsub plugin is available
    await app.pubsub.subscribe(topicName, `${topicName}-subscription`, messageHandler);

    // Run the test
    await doRequestAndCheckResponse({
      t,
      app,
      file: {
        method: 'PUT',
        type: 'ecgs',
        name: fileName,
        payload: testPayload,
      },
      deviceSerial,
      patientId: '',
      expectedResponse: {},
    });

    // Wait for the message to come through and check the contents in the callbacks
    await messagePromise;
  });

  // ==============================================================================
  d.test('Valid and invalid creation requests via pubsub', async (t) => {
    // NOTE: This test requires a running redis instance at localhost. See the readme
    // for some options to set that up.
    const topicName = 'test-actions-queue';
    const pubsubOptions = {
      enabled: true,
      actionsTopic: topicName,
      subscription: `${topicName}-subscription`,
    };
    const app = buildFastify({}, { redisMaster: 'localhost', pubsub: pubsubOptions });

    // This test has 6 expects
    t.plan(6);

    const deletePayload = {
      format: 'DELETE',
      deviceSerial: testActionsJson.deviceSerial,
    };

    // At the end of your tests it is highly recommended to call `.close()`
    // to ensure that all connections to external services get closed.
    t.teardown(async () => {
      // A 'backup' delete all, in case the first expects fail...
      await app.inject({
        method: 'POST',
        url: '/queue',
        payload: deletePayload,
      });
      app.close();
    });

    await app.inject({
      method: 'POST',
      url: '/queue',
      payload: deletePayload,
    });

    // Publish the bad action
    await app.pubsub.publish(topicName, JSON.stringify(testEventJson));

    // Wait for the pubsub message to come through...
    await sleep(500);

    await doRequestAndCheckResponse({
      t,
      app,
      file: { method: 'GET', type: 'actions' },
      deviceSerial: testActionsJson.deviceSerial,
      patientId: '',
      expectedResponse: {},
    });

    // Publish the valid action
    await app.pubsub.publish(topicName, JSON.stringify(testActionsJson));

    // Wait for the pubsub message to come through...
    await sleep(500);

    await doRequestAndCheckResponse({
      t,
      app,
      file: { method: 'GET', type: 'settings' },
      deviceSerial: testActionsJson.deviceSerial,
      patientId: '',
      expectedResponse: testActionsJson,
      parser: tza,
    });
  });
});
