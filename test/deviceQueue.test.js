/* eslint-env node */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

const tap = require('tap');
const fs = require('fs');
const { tza, tzs } = require('@tzmedical/trident-sdk');

const buildFastify = require('../app');
const { doRequestAndCheckResponse } = require('./helper');

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

const testActionsJson = require('./testActions.json');
const testEventJson = require('./testEvent.json');
const testSettingsJson = require('./testSettings.json');
const testReportJson = require('./testReport.json');
const testActionEnrollmentIdJson = require('./testActionEnrollmentId.json');

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

// ==============================================================================
async function deleteQueue(app, deviceSerial) {
  const deletePayload = {
    format: 'DELETE',
    deviceSerial,
  };
  return app.inject({
    method: 'POST',
    url: '/queue',
    payload: deletePayload,
  });
}

// ==============================================================================
async function addToQueue(t, app, deviceSerial, content, count = 1) {
  const apiResponse = await app.inject({
    method: 'POST',
    url: '/queue',
    payload: content,
  });
  t.equal(apiResponse.statusCode, 200, 'POST /queue should return a status code of 200');
  const queueExpected = JSON.stringify({ deviceSerial, queueCount: count });
  t.equal(
    apiResponse.body,
    queueExpected,
    'POST /queue should return the number of queued items for the device'
  );
}

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

// ==============================================================================
tap.test('sending a queued action in response to a PUT request', async (t) => {
  // NOTE: This test requires a running redis instance at localhost. See the readme
  // for some options to set that up.
  const app = buildFastify({}, { redisMaster: 'localhost', redisReplicas: 'localhost' });

  // This test has 11 expects
  t.plan(11);

  const testDevicePayload = fs.readFileSync(`${__dirname}/testEvent.tze`);
  const { deviceSerial } = testEventJson;
  const { sequence } = testEventJson;
  const count = '001';
  const fileName = `${deviceSerial}_${sequence}_${count}.tze`;

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(async () => {
    // A 'backup' delete all, in case the first expects fail...
    deleteQueue(app, deviceSerial);
    app.close();
  });

  // Queue the actions file
  await deleteQueue(app, deviceSerial);
  await addToQueue(t, app, deviceSerial, testActionsJson);

  // Send the request from the device
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'events',
      name: fileName,
      payload: testDevicePayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: testActionsJson,
    parser: tza,
  });

  // Send the request again to confirm caching works
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'events',
      name: fileName,
      payload: testDevicePayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: testActionsJson,
    parser: tza,
  });

  // Then do a new file to confirm that the queue is empty
  await doRequestAndCheckResponse({
    t,
    app,
    file: { method: 'GET', type: 'actions' },
    deviceSerial,
    patientId: '',
    expectedResponse: {},
  });
});

// ==============================================================================
tap.test('sending a queued settings in response to a PUT request', async (t) => {
  // NOTE: This test requires a running redis instance at localhost. See the readme
  // for some options to set that up.
  const app = buildFastify({}, { redisMaster: 'localhost', redisReplicas: 'localhost' });

  // This test has 11 expects
  t.plan(11);

  const testDevicePayload = fs.readFileSync(`${__dirname}/testReport.tzr`);
  const { deviceSerial } = testReportJson;
  const dateTime = testReportJson.events[0].datetime
    .substring(0, 13)
    .replace(/-/g, '')
    .replace(/ /g, '_');
  const fileName = `${deviceSerial}_${dateTime}.tzr`;

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(async () => {
    // A 'backup' delete all, in case the first expects fail...
    deleteQueue(app, deviceSerial);
    app.close();
  });

  // Queue the settings file
  await deleteQueue(app, deviceSerial);
  await addToQueue(t, app, deviceSerial, testSettingsJson);

  // Send the request from the device
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'reports',
      name: fileName,
      payload: testDevicePayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: testSettingsJson,
    parser: tzs,
  });

  // Send the request again to confirm caching works
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'reports',
      name: fileName,
      payload: testDevicePayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: testSettingsJson,
    parser: tzs,
  });

  // Then do a new file to confirm that the queue is empty
  await doRequestAndCheckResponse({
    t,
    app,
    file: { method: 'GET', type: 'settings' },
    deviceSerial,
    patientId: '',
    expectedResponse: {},
  });
});

// ==============================================================================
tap.test('sending a queued action in response to a GET request', async (t) => {
  // NOTE: This test requires a running redis instance at localhost. See the readme
  // for some options to set that up.
  const app = buildFastify({}, { redisMaster: 'localhost' });

  // This test has 11 expects
  t.plan(11);

  const testDevicePayload = fs.readFileSync(`${__dirname}/testEvent.tze`);
  const { deviceSerial } = testEventJson;
  const { sequence } = testEventJson;
  const count = '001';
  const fileName = `${deviceSerial}_${sequence}_${count}.tze`;

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(async () => {
    // A 'backup' delete all, in case the first expects fail...
    deleteQueue(app, deviceSerial);
    app.close();
  });

  // Queue the actions file
  await deleteQueue(app, deviceSerial);
  await addToQueue(t, app, deviceSerial, testActionsJson);

  // Send the request from the device
  await doRequestAndCheckResponse({
    t,
    app,
    file: { method: 'GET', type: 'actions' },
    deviceSerial,
    patientId: '',
    expectedResponse: testActionsJson,
    parser: tza,
  });

  // Send the request again to confirm caching works
  await doRequestAndCheckResponse({
    t,
    app,
    file: { method: 'GET', type: 'actions' },
    deviceSerial,
    patientId: '',
    expectedResponse: testActionsJson,
    parser: tza,
  });

  // Then do a new file to confirm that the queue is empty
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'events',
      name: fileName,
      payload: testDevicePayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: {},
  });
});

// ==============================================================================
tap.test('sending a queued settings in response to a GET request', async (t) => {
  // NOTE: This test requires a running redis instance at localhost. See the readme
  // for some options to set that up.
  const app = buildFastify({}, { redisMaster: 'localhost' });

  // This test has 11 expects
  t.plan(11);

  const testDevicePayload = fs.readFileSync(`${__dirname}/testReport.tzr`);
  const { deviceSerial } = testReportJson;
  const dateTime = testReportJson.events[0].datetime
    .substring(0, 13)
    .replace(/-/g, '')
    .replace(/ /g, '_');
  const fileName = `${deviceSerial}_${dateTime}.tzr`;

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(async () => {
    // A 'backup' delete all, in case the first expects fail...
    deleteQueue(app, deviceSerial);
    app.close();
  });

  // Queue the settings file
  await deleteQueue(app, deviceSerial);
  await addToQueue(t, app, deviceSerial, testSettingsJson);

  // Send the request from the device
  await doRequestAndCheckResponse({
    t,
    app,
    file: { method: 'GET', type: 'actions' },
    deviceSerial,
    patientId: '',
    expectedResponse: testSettingsJson,
    parser: tzs,
  });

  // Send the request again to confirm caching works
  await doRequestAndCheckResponse({
    t,
    app,
    file: { method: 'GET', type: 'actions' },
    deviceSerial,
    patientId: '',
    expectedResponse: testSettingsJson,
    parser: tzs,
  });

  // Then do a new file to confirm that the queue is empty
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'reports',
      name: fileName,
      payload: testDevicePayload,
    },
    deviceSerial,
    patientId: '',
    expectedResponse: {},
  });
});

// ==============================================================================
tap.test('limiting queued actions based on patientId', async (t) => {
  // NOTE: This test requires a running redis instance at localhost. See the readme
  // for some options to set that up.
  const app = buildFastify({}, { redisMaster: 'localhost' });

  // This test has 20 expects
  t.plan(20);

  const testApiPayload3 = { ...testActionsJson };
  const testDevicePayload = fs.readFileSync(`${__dirname}/testEvent.tze`);
  const testDeviceEnrollmentId = testEventJson.patientId;
  const { deviceSerial } = testEventJson;
  const { sequence } = testEventJson;
  const count = '001';
  const fileName = `${deviceSerial}_${sequence}_${count}.tze`;

  testActionsJson.patientId = 'booger';
  testApiPayload3.patientId = testDeviceEnrollmentId;

  // At the end of your tests it is highly recommended to call `.close()`
  // to ensure that all connections to external services get closed.
  t.teardown(async () => {
    // A 'backup' delete all, in case the first expects fail...
    deleteQueue(app, deviceSerial);
    app.close();
  });

  // Queue the actions files
  await deleteQueue(app, deviceSerial);
  await addToQueue(t, app, deviceSerial, testActionsJson, 1);
  await addToQueue(t, app, deviceSerial, testActionEnrollmentIdJson, 2);

  // Send the request from the device
  await doRequestAndCheckResponse({
    t,
    app,
    file: { method: 'GET', type: 'actions' },
    deviceSerial,
    patientId: testDeviceEnrollmentId,
    expectedResponse: testActionEnrollmentIdJson,
    parser: tza,
  });

  // Send the request again to confirm caching works
  await doRequestAndCheckResponse({
    t,
    app,
    file: { method: 'GET', type: 'actions' },
    deviceSerial,
    patientId: testDeviceEnrollmentId,
    expectedResponse: testActionEnrollmentIdJson,
    parser: tza,
  });

  // Then do a new file to confirm that the queue is empty
  await doRequestAndCheckResponse({
    t,
    app,
    file: {
      method: 'PUT',
      type: 'events',
      name: fileName,
      payload: testDevicePayload,
    },
    deviceSerial,
    patientId: testDeviceEnrollmentId,
    expectedResponse: {},
  });

  // Queue one more set, this time with a matching enrollmentId
  testActionEnrollmentIdJson.patientId = 'booger';
  await addToQueue(t, app, deviceSerial, testActionEnrollmentIdJson, 1);
  await addToQueue(t, app, deviceSerial, testApiPayload3, 2);

  // Send the request from the device
  await doRequestAndCheckResponse({
    t,
    app,
    file: { method: 'GET', type: 'actions' },
    deviceSerial,
    patientId: testDeviceEnrollmentId,
    expectedResponse: testApiPayload3,
    parser: tza,
  });
});
