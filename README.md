# Trident Server

[![npm version](https://img.shields.io/npm/v/@tzmedical/trident-server.svg?logo=npm)](https://www.npmjs.org/package/@tzmedical/trident-server)
[![Docker Version](https://img.shields.io/docker/v/tzmedical/trident-server?logo=Docker)](https://hub.docker.com/r/tzmedical/trident-server)
[![node version](https://img.shields.io/node/v/@tzmedical/trident-server?logo=Node.js)](https://nodejs.org/en/)
[![fastify version](https://img.shields.io/npm/dependency-version/@tzmedical/trident-server/fastify?logo=Fastify)](https://www.fastify.io/)

[![CircleCI](https://img.shields.io/circleci/build/bitbucket/tzmedical/trident-server?logo=CircleCI)](https://app.circleci.com/pipelines/bitbucket/tzmedical/trident-server)
[![npm downloads](https://img.shields.io/npm/dm/@tzmedical/trident-server?logo=npm)](https://www.npmjs.org/package/@tzmedical/trident-server)
[![Docker Pulls](https://img.shields.io/docker/pulls/tzmedical/trident-server?logo=Docker)](https://hub.docker.com/r/tzmedical/trident-server)

This repository contains an example Node.js server designed to receive and
process data uploaded from TZ Medical's Trident line of cardiac monitors.

# Deployment

This server application can be deployed several different ways depending on your
requirements. It can be run directly as an npm package, or it can be pulled as a
docker image and run in any environment that supports containers. There is also
a helm chart is available for kubernetes deployments.

## Node.js

To run the server as an npm package, you can do one of the following:

```
# Install the package globally
npm i -g @tzmedical/trident-server
# Start the server on port 80
trident-server start -p 80

### OR ###

# Run the server using npx at the default port (3000)
npx @tzmedical/trident-server start
```

## Docker

A docker image containing this server application is available on Docker Hub at
[tzmedical/trident-server](https://hub.docker.com/r/tzmedical/trident-server).
It can be pulled using the following command:

```
# Pull the application Docker image
docker pull tzmedical/trident-server
```

## Helm

The Helm chart is available from Google Cloud Storage. To use it to install the
server in your cluster, use the following commands to install as
`my-trident-server`:

```
# Add the repository
helm repo add trident-server https://storage.googleapis.com/trident-server
# Install the chart
helm install my-trident-server trident-server/trident-server
```

# Configuration

The following configuration options are available as either cli flags or environment variables:

| CLI flag                | Environment Variable             | Helm Chart             | Default                        | Description                                             |
| ----------------------- | -------------------------------- | ---------------------- | ------------------------------ | ------------------------------------------------------- |
| `-p, --port`            | `NODE_PORT`                      | `service.port`         | `80`                           | Sets the port for the application to listen on          |
| `--post`                | `FORWARDING_URL_LIST`            | `forwardingUrls`       | `[]`                           | Sets a list of URLs to POST the parsed file contents to |
|                         | `FORWARDING_TIMEOUT_MS`          | `forwardingTimeoutMs`  | `30000`                        | Sets the timeout for forwarding POST requests           |
| `--redis-master`        | `REDIS_MASTER_HOST`              | -automatic-            | `''`                           | Sets the hostname for the Redis master instance         |
| `--redis-replicas`      | `REDIS_REPLICAS_HOST`            | -automatic-            | `''`                           | Sets the hostname for the Redis replicas                |
| `--redis-password`      | `REDIS_PASSWORD`                 | -automatic-            | `''`                           | Sets the password                                       |
| `--log-level`           | `LOG_LEVEL`                      | `logLevel`             | `'info'`                       | Sets the logging level for the application server       |
| `--pretty-logs`         | `PRETTY_FORMAT_LOGS`             | `prettyLogs`           | `false`                        | Set to use pretty-print formatting for logs             |
| `--pubsub-enabled`      | `PUBSUB_ENABLED`                 | `pubsub.enabled`       | `''`                           | Set to enable use of google-pubsub                      |
| `--pubsub-received`     | `PUBSUB_RECEIVED_TOPIC`          | `pubsub.receivedTopic` | `trident-server-file-received` | Set to override the default topic name                  |
| `--pubsub-actions`      | `PUBSUB_ACTIONS_TOPIC`           | `pubsub.actionsTopic`  | `trident-server-actions-queue` | Set to override the default topic name                  |
| `--pubsub-subscription` | `PUBSUB_SUBSCRIPTION`            | `pubsub.subscription`  | `trident-server-actions-queue` | Set to override the default subscription name           |
|                         | `GOOGLE_APPLICATION_CREDENTIALS` | `googleCredentials`    |                                | Set to provide Google application credentials           |

# Development

When developing and testing locally, a Redis server is needed. This is also
required for `npm test` to succeed, as it expects to find a Redis instance on
the default port at `localhost`. There are a number of options to achieve this.

- [Memurai Developer
  (Windows)](https://community.chocolatey.org/packages/memurai-developer.install):
  The official Windows version requires WSL and is complicated to install.
  Instead, Memurai can be quickly installed via chocolatey. By default, it
  creates a Windows service so you can be up and running quickly.
- [Official Redis Sever
  (Linux)](https://redis.io/docs/getting-started/installation/install-redis-on-linux/):
  On Linux, the redis server can be installed using `apt-get` relatively easily.
- [Using a Docker Container (Any)](https://hub.docker.com/_/redis): There is a
  Redis image available on Docker Hub that can be used as well. This is what our
  automated testing uses.

Local testing also requires an emulator for Google's pubsub service. This can
be installed with the following commands:

```
gcloud components install beta pubsub-emulator --quiet
gcloud components update --quiet
```
