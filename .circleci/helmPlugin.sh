#!/bin/bash

if ! helm plugin list | grep -q "secrets"; then
  helm plugin add https://github.com/jkroepke/helm-secrets --version v3.12.0
fi