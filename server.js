/* eslint-env node */

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

// Require library to exit fastify process, gracefully (if possible)
const closeWithGrace = require('close-with-grace');

const Server = require('./app');

// Bootstrap environment variables from the .env file
require('dotenv').config();

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

// ==============================================================================
const start = async ({
  // Server options
  port,
  prettyLogs,
  logLevel,
  postList,
  redisMaster,
  redisReplicas,
  redisPassword,
  pubsub,
}) => {
  // Set our logging options to be prettier when developing
  const fastifyOptions = {
    logger: {
      level: logLevel || process.env.LOG_LEVEL || 'info',
    },
  };
  if (prettyLogs || process.env.PRETTY_FORMAT_LOGS) {
    fastifyOptions.logger.prettyPrint = true;
  }

  // Set our application options up
  const applicationOptions = {
    postList,
    redisMaster,
    redisReplicas,
    redisPassword,
    pubsub,
  };

  // Build the server
  const server = Server(fastifyOptions, applicationOptions);

  // Setup graceful shutdown hook
  // delay is the number of milliseconds for the graceful close to finish
  const closeListeners = closeWithGrace({ delay: 500 }, async ({ err }) => {
    if (err) {
      server.log.error(err);
    }
    await server.close();
  });
  server.addHook('onClose', async (instance, done) => {
    closeListeners.uninstall();
    done();
  });

  // Start the server
  try {
    await server.listen({ port: port || process.env.NODE_PORT, host: '0.0.0.0' });
  } catch (err) {
    server.log.error(err);
    process.exit(1);
  }
};

module.exports = start;
